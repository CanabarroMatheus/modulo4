db.createCollection("banco")
/*db.banco.find()*/

var enderecoDefault = {
    logradouro: "Testando",
    numero: "1",
    complemento: "",
    cep: "90000000",
    bairro: "NA",
    cidade: "NA",
    estado: "NA",
    pais: "Brasil"
}

var gerenteDefault = {
    codigoFuncionario: "01",
    tipoGerente: "GG",
    nome: "Anderson",
    CPF: "72663153010",
    estadoCivil: "casado",
    dataDeNascimento: "2001-01-01",
    endereco: enderecoDefault
}

var cliente1 = {
    nome: "Julio Cezar",
    CPF: "63465457013",
    estadoCivil: "Casado",
    dataDeNascimento: "2001-01-01",
    endereco: enderecoDefault
}

var cliente2 = {
    nome: "Fabiano",
    CPF: "77152072094",
    estadoCivil: "Solteiro",
    dataDeNascimento: "2001-01-01",
    endereco: enderecoDefault
}

var cliente3 = {
    nome: "João",
    CPF: "03445988021",
    estadoCivil: "Solteiro",
    dataDeNascimento: "2001-01-01",
    endereco: enderecoDefault
}

var bancoAlfa = {
    codigo: "011",
    nome: "Alfa",
    agencias: [
        {
            codigo: "0001",
            nome: "Web",
            endereco: {
                logradouro: "Rua Testando",
                numero: "55",
                complemento: "loja 1",
                cep: "900000000",
                bairro: "NA",
                cidade: "NA",
                estado: "NA",
                pais: "Brasil"
            },
            consolidacao: {
                saldoAtual: 0,
                saques: 0,
                depositos: 0,
                numeroCorrentistas: 0
            },
            contas: [
                {
                    codigo: "1",
                    tipoConta: "PJ",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1, cliente2, cliente3]
                },
                {
                    codigo: "2",
                    tipoConta: "PJ",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1, cliente2, cliente3]
                },
                {
                    codigo: "3",
                    tipoConta: "CONJ",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1, cliente2]
                },
                {
                    codigo: "4",
                    tipoConta: "CONJ",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1, cliente2]
                }
            ]
        },
        {
            codigo: "0002",
            nome: "California",
            endereco: {
                logradouro: "Testing",
                numero: "122",
                complemento: "",
                cep: "90000000",
                bairro: "Between Hyde and Powell Streets",
                cidade: "San Francisco",
                estado: "California",
                pais: "England"
            },
            consolidacao: {
                saldoAtual: 0,
                saques: 0,
                depositos: 0,
                numeroCorrentistas: 0
            },
            contas: [
                {
                    codigo: "5",
                    tipoConta: "CONJ",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1, cliente2]
                },
                {
                    codigo: "6",
                    tipoConta: "PF",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1]
                },
                {
                    codigo: "7",
                    tipoConta: "PF",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1]
                }
            ]
        },
        {
            codigo: "0101",
            nome: "Londres",
            endereco: {
                logradouro: "Testing",
                numero: "525",
                complemento: "",
                cep: "90000000",
                bairro: "Croydon",
                cidade: "Londres",
                estado: "Boroughs",
                pais: "England"
            },
            consolidacao: {
                saldoAtual: 0,
                saques: 0,
                depositos: 0,
                numeroCorrentistas: 0
            },
            contas: [
                {
                    codigo: "8",
                    tipoConta: "PF",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1]
                },
                {
                    codigo: "9",
                    tipoConta: "PF",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1]
                },
                {
                    codigo: "10",
                    tipoConta: "PF",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1]
                },
            ]
        }
    ]
}

var bancoBeta = {
    codigo: "241",
    nome: "Beta",
    agencias: [
        {
            codigo: "0001",
            nome: "Web",
            endereco: {
                logradouro: "Testando",
                numero: "55",
                complemento: "loja 2",
                cep: "90000000",
                bairro: "NA",
                cidade: "NA",
                estado: "NA",
                pais: "Brasil"
            },
            consolidacao: {
                saldoAtual: 0,
                saques: 0,
                depositos: 0,
                numeroCorrentistas: 0
            },
            contas: [
                {
                    codigo: "11",
                    tipoConta: "PJ",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1, cliente2, cliente3]
                },
                {
                    codigo: "12",
                    tipoConta: "PJ",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1, cliente2, cliente3]
                },
                {
                    codigo: "13",
                    tipoConta: "CONJ",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1, cliente2]
                },
                {
                    codigo: "14",
                    tipoConta: "CONJ",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1, cliente2]
                },
                {
                    codigo: "15",
                    tipoConta: "CONJ",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1, cliente2]
                },
                {
                    codigo: "16",
                    tipoConta: "PF",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1]
                },
                {
                    codigo: "17",
                    tipoConta: "PF",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1]
                },
                {
                    codigo: "18",
                    tipoConta: "PF",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1]
                },
                {
                    codigo: "19",
                    tipoConta: "PF",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1]
                },
                {
                    codigo: "20",
                    tipoConta: "PF",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1]
                }
            ]
        }
    ]
}

var bancoOmega = {
    codigo: "307",
    nome: "Omega",
    agencias: [
        {
            codigo: "0001",
            nome: "Web",
            endereco: {
                logradouro: "Testando",
                numero: "55",
                complemento: "loja 3",
                cep: "90000000",
                bairro: "NA",
                cidade: "NA",
                estado: "NA",
                pais: "Brasil"
            },
            consolidacao: {
                saldoAtual: 0,
                saques: 0,
                depositos: 0,
                numeroCorrentistas: 0
            },
            contas: [
                {
                    codigo: "21",
                    tipoConta: "PJ",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1, cliente2, cliente3]
                },
                {
                    codigo: "22",
                    tipoConta: "PJ",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1, cliente2, cliente3]
                },
                {
                    codigo: "23",
                    tipoConta: "CONJ",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1, cliente2]
                },
                {
                    codigo: "24",
                    tipoConta: "CONJ",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1, cliente2]
                }
            ]
        },
        {
            codigo: "8761",
            nome: "Itu",
            endereco: {
                logradouro: "do Meio",
                numero: "2233",
                complemento: "",
                cep: "90000000",
                bairro: "Qualquer",
                cidade: "Itu",
                estado: "São Paulo",
                pais: "Brasil"
            },
            consolidacao: {
                saldoAtual: 0,
                saques: 0,
                depositos: 0,
                numeroCorrentistas: 0
            },
            contas: [
                {
                    codigo: "25",
                    tipoConta: "CONJ",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1, cliente2]
                },
                {
                    codigo: "26",
                    tipoConta: "PF",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1]
                },
                {
                    codigo: "27",
                    tipoConta: "PF",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1]
                }
            ]
        },
        {
            codigo: "4567",
            nome: "Hermana",
            endereco: {
                logradouro: "do Boca",
                numero: "222",
                complemento: "",
                cep: "90000000",
                bairro: "Caminto",
                cidade: "Buenos Aires",
                estado: "Buenos Aires",
                pais: "Argentina"
            },
            consolidacao: {
                saldoAtual: 0,
                saques: 0,
                depositos: 0,
                numeroCorrentistas: 0
            },
            contas: [
                {
                    codigo: "28",
                    tipoConta: "PF",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1]
                },
                {
                    codigo: "29",
                    tipoConta: "PF",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1]
                },
                {
                    codigo: "30",
                    tipoConta: "PF",
                    saldo: 0.00,
                    gerentes: [gerenteDefault],
                    clientes: [cliente1]
                },
            ]
        }
    ]
}
    
db.banco.insert(bancoAlfa);

db.banco.insert(bancoBeta);

db.banco.insert(bancoOmega);

/*db.banco.drop()*/
db.banco.find().pretty()

//buscas

db.banco.find({nome: "Beta"})//equals
db.banco.find({nome: {$regex: /lf/}}) //Contains (regex)
db.banco.find({codigo: {$eq: "011"}}) //Equals
db.banco.find({codigo: {$nq: "011"}}) //Not equals
db.banco.find({"agencia.nome": "WEB"}) //Composto
db.banco.find({"agencia.conta.movimentacoes.valor": {$gt: 10}})//Maior que (>)
db.banco.find({"agencia.conta.movimentacoes.valor": {$lt: 10}})//Menor que (<)
db.banco.find({"agencia.conta.movimentacoes.valor": {$gte: 10}})//Maior igual que (>=) 
db.banco.find({"agencia.conta.movimentacoes.valor": {$lte: 10}})//Menor igual que (<=)
//db.banco.find({"agencia.conta.movimentacoes.valor": {$in: {10, 100}}})//Menor igual que (<=)
db.banco.find().sort(); // order by ASC
db.banco.find().sort({"agencia.conta.movimentacoes.valor": -1}); // order by DESC
