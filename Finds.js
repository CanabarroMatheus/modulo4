db.banco.find();

db.banco.update({_id: ObjectId("60ba743d621db31ab00c9e2e")}, {$set: {nome: "Alfa"}});

/*
    $set - Inserir um valor no campo
    $unset - Deletar o campo
    $inc - Incrementar o valor do campo
    $mult - Multiplicar o valor do campo
    $rename - Renomear o campo
*/

db.banco.deleteOne({nome: "Alfa"});
db.banco.deleteMany({});

