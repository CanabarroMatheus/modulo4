db.createCollection("movimentacao");
db.createCollection("endereco");
db.createCollection("pais");
db.createCollection("estado");
db.createCollection("cidade");
db.createCollection("bairro");
db.createCollection("estadoCivil");
db.createCollection("tipoGerente");
db.createCollection("pessoa");
db.createCollection("agencia");
db.createCollection("banco");
db.createCollection("consolidacao");
db.createCollection("conta");

db.movimentacao.insert({
    "tipo": "credito",
    "valor": 1200.0
});

db.movimentacao.insert({
    "tipo": "debito",
    "valor": 100.0
});

db.pais.insert({
    "nome": "Brasil"
});

db.pais.insert({
    "nome": "Argentina"
});

db.pais.insert({
    "nome": "Estados Unidos"
});

db.pais.insert({
    "nome": "England"
});

db.estado.insert({
    "idPais": ObjectId("60bfb2759470d80671e2a2bb"),
    "nome": "Rio Grande do Sul"
});

db.estado.insert({
    "idPais": ObjectId("60bfb2759470d80671e2a2bb"),
    "nome": "NA"
});

db.estado.insert({
    "idPais": ObjectId("60bfb2759470d80671e2a2bc"),
    "nome": "Buenos Aires"
});

db.estado.insert({
    "idPais": ObjectId("60bfcb979470d80671e2a2db"),
    "nome": "California"
});

db.estado.insert({
    "idPais": ObjectId("60bfcccb9470d80671e2a2df"),
    "nome": "Boroughs"
});

db.cidade.insert({
    "idEstado": ObjectId("60bfb4ca9470d80671e2a2bf"),
    "nome": "Porto Alegre"
});

db.cidade.insert({
    "idEstado": ObjectId("60bfc4519470d80671e2a2d1"),
    "nome": "NA"
});

db.cidade.insert({
    "idEstado": ObjectId("60bfb4ca9470d80671e2a2c0"),
    "nome": "Buenos Aires" 
});

db.cidade.insert({
    "idEstado": ObjectId("60bfcc149470d80671e2a2dc"),
    "nome": "San Francisco" 
});

db.cidade.insert({
    "idEstado": ObjectId("60bfcd0f9470d80671e2a2e0"),
    "nome": "Londres" 
});

db.bairro.insert({
    "idCidade": ObjectId("60bfb5599470d80671e2a2c1"),
    "nome": "Rio Branco"
});

db.bairro.insert({
    "idCidade": ObjectId("60bfc5989470d80671e2a2d2"),
    "nome": "NA"
});

db.bairro.insert({
    "idCidade": ObjectId("60bfb5599470d80671e2a2c2"),
    "nome": "Barracas"
});

db.bairro.insert({
    "idCidade": ObjectId("60bfcc4f9470d80671e2a2dd"),
    "nome": "Between Hyde and Powell Streets"
});

db.bairro.insert({
    "idCidade": ObjectId("60bfcd4a9470d80671e2a2e1"),
    "nome": "Croydon"
});

db.endereco.insert({
    "idBairro": ObjectId("60bfb6559470d80671e2a2c3"),
    "logradouro": "São Manoel",
    "numero": "808",
    "complemento": "Ap 402",
    "cep": "92620110"
});

db.endereco.insert({
    "idBairro": ObjectId("60bfc5c39470d80671e2a2d3"),
    "logradouro": "Testando",
    "numero": "55",
    "complemento": "loja 1",
    "cep": "90000000"
});

db.endereco.insert({
    "idBairro": ObjectId("60bfb6559470d80671e2a2c4"),
    "logradouro": "Alvarado",
    "numero": "2440",
    "complemento": "",
    "cep": "00000000"
});

db.endereco.insert({
    "idBairro": ObjectId("60bfcd829470d80671e2a2e2"),
    "logradouro": "Testing",
    "numero": "525",
    "complemento": "",
    "cep": "00000000"
});

db.endereco.insert({
    "idBairro": ObjectId("60bfcc9a9470d80671e2a2de"),
    "logradouro": "Testing",
    "numero": "122",
    "complemento": "",
    "cep": "00000000"
});

db.banco.insert({
    "nome": "Alfa"
});

db.banco.insert({
    "nome": "Beta"
});

db.banco.insert({
    "nome": "Omega"
});

db.agencia.insert({
    "idBanco": ObjectId("60bfbe1c9470d80671e2a2ce"),
    "idEndereco": ObjectId("60bfc65a9470d80671e2a2d4"),
    "nome": "Web"
});

db.agencia.insert({
    "idBanco": ObjectId("60bfbe1c9470d80671e2a2ce"),
    "idEndereco": ObjectId("60bfce719470d80671e2a2e4"),
    "nome": "California"
});

db.agencia.insert({
    "idBanco": ObjectId("60bfbe1c9470d80671e2a2ce"),
    "idEndereco": ObjectId("60bfcdce9470d80671e2a2e3"),
    "nome": "Londres"
});

db.agencia.insert({
    "idBanco": ObjectId(""),
    "idEndereco": ObjectId(""),
    "nome": ""
});

db.agencia.insert({
    "idBanco": ObjectId(""),
    "idEndereco": ObjectId(""),
    "nome": ""
});

db.agencia.insert({
    "idBanco": ObjectId(""),
    "idEndereco": ObjectId(""),
    "nome": ""
});

db.agencia.insert({
    "idBanco": ObjectId(""),
    "idEndereco": ObjectId(""),
    "nome": ""
});

db.estadoCivil.insert({
    "status": "Solteiro" 
});

db.estadoCivil.insert({
    "status": "Casado" 
});

db.estadoCivil.insert({
    "status": "Divorciado" 
});

db.estadoCivil.insert({
    "status": "União Estável" 
});

db.estadoCivil.insert({
    "status": "Viuvo" 
});

db.tipoGerente.insert({
    "tipo": "Gerente geral",
    "sigla": "GG"
});

db.tipoGerente.insert({
    "tipo": "Gerente comum",
    "sigla": "GC"
});

db.pessoa.insert({
    "idEndereco": ObjectId("60bfb7d79470d80671e2a2c5"),
    "idEstadoCivil": ObjectId("60bfba709470d80671e2a2c7"),
    "nome": "João",
    "dataNascimento": "2000-01-01",
    "cpf": "19752601049"
});

db.pessoa.insert({
    "idEndereco": ObjectId("60bfb7d79470d80671e2a2c5"),
    "idEstadoCivil": ObjectId("60bfba709470d80671e2a2c7"),
    "nome": "José",
    "dataNascimento": "2000-01-01",
    "cpf": "46137119084"
});

db.pessoa.insert({
    "idEndereco": ObjectId("60bfb7d79470d80671e2a2c5"),
    "idEstadoCivil": ObjectId("60bfba709470d80671e2a2c7"),
    "nome": "Otávio",
    "dataNascimento": "2000-01-01",
    "cpf": "28996660027"
});

db.pessoa.insert({
    "idAgencia": ObjectId("60bfc6ac9470d80671e2a2d5"),
    "idEndereco": ObjectId("60bfb7d79470d80671e2a2c5"),
    "idTipoGerente": ObjectId("60bfbb499470d80671e2a2cd"),
    "nome": "Marcos Henrique",
    "dataNascimento": "2000-01-01",
    "codigoFuncionario": "12345-6",
    "cpf": "85782782080"
});

db.pessoa.insert({
    "idAgencia": ObjectId("60bfc6ac9470d80671e2a2d5"),
    "idEndereco": ObjectId("60bfb7d79470d80671e2a2c5"),
    "idTipoGerente": ObjectId("60bfbb499470d80671e2a2cc"),
    "nome": "Marcelo D2",
    "dataNascimento": "2000-01-01",
    "codigoFuncionario": "23456-7",
    "cpf": "07438205096"
});



db.movimentacao.find({});
db.pais.find({});
db.estado.find({});
db.cidade.find({});
db.bairro.find({});
db.endereco.find({});
db.estadoCivil.find({});
db.tipoGerente.find({});
db.pessoa.find({});
db.agencia.find({});
db.banco.find({});

db.movimentacao.drop();
db.pais.drop();
db.estado.drop();
db.cidade.drop();
db.bairro.drop();
db.endereco.drop();
db.agencia.drop();
db.banco.drop();